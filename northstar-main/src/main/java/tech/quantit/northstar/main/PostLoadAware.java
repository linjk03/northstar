package tech.quantit.northstar.main;

public interface PostLoadAware {

	void postLoad();
}
