package tech.quantit.northstar.strategy.api.indicator.complex;

/**
 * 本目录是对复杂的指标做进一步封装方便调用
 * 
 * 该目录下的类名称为了与常用指标保持一致，并没有僵硬地采用驼峰命名
 */